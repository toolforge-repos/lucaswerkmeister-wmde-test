import flask
from markupsafe import Markup
import os
import toolforge
import yaml

config_path = 'config.yaml'
if 'TOOL_DATA_DIR' in os.environ:
    print(f'We’re on Toolforge! {os.environ["TOOL_DATA_DIR"]}/www/python/src/config.yaml is the new config path')
    config_path = os.environ['TOOL_DATA_DIR'] + '/www/python/src/config.yaml'

app = flask.Flask(__name__)

have_config = app.config.from_file(
    config_path,
    load=yaml.safe_load,
    silent=True,
)
print(f'{have_config=}')

user_agent = toolforge.set_user_agent(
    'lucaswerkmeister-wmde-test',
    email='tools.lucaswerkmeister-wmde-test@toolforge.org',
)

@app.get('/')
def index() -> Markup:
    return Markup(r'Hello, world!')

@app.get('/has-secret-key')
def has_secret_key() -> Markup:
    if app.secret_key is None:
        return Markup(r'We do not have a secret key :(')
    else:
        return Markup(r'Looks like we have a secret key \o/')
